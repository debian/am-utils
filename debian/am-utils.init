#! /bin/sh

#  Copyright (C) 2004-2008 Tim Cutts <timc@chiark.greenend.org.uk>
#  Copyright (C) 2001-2003 Philippe Troin <phil@fifi.org>
#  Copyright (C) 1996-98 by Dominik Kubla, <kubla@Uni-Mainz.DE> and
#                        Roman Hodek <Roman.Hodek@informatik.uni-erlangen.de>
#

### BEGIN INIT INFO
# Provides:          am-utils
# Required-Start:    $portmap $remote_fs $syslog
# Required-Stop:     $portmap $remote_fs $syslog
# Should-Start:      ypbind nslcd
# Should-Stop:       ypbind nslcd
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: BSD automounter
# Description:       Automount local and remote filesystems
### END INIT INFO

PATH=/usr/sbin:/sbin:/usr/bin:/bin
export PATH

# Defaults
CONF=/etc/default/am-utils

# If the package is removed, skip it.
test -x /usr/sbin/amd || exit 0

get_amd_args() {
    if [ ! -f "$CONF" ]
    then
	echo "$0: not configured yet (try running \"dpkg-reconfigure am-utils\")." 1>&2
	exit 1
    fi
    . "$CONF"

    if test "$AM_UTILS_USE_NIS" = true
    then
	map="$AM_UTILS_NIS_MASTER_MAP"
	case "$AM_UTILS_NIS_MASTER_MAP_KEY_STYLE" in
	    config)
		key="$AM_UTILS_NIS_KEY"
		AMDARGS="`ypmatch \"$key\" \"$map\"`"
		;;
	    onekey)
		AMDARGS="`ypcat \"$map\"`"
		;;
	    mountpoint)
		AMDARGS="`ypcat -k \"$map\"`"
		;;
	    custom)
		AMDARGS="`sh -c \"$AM_UTILS_NIS_CUSTOM\"`"
		;;
	    *)
		echo "$0: bad key style"
		exit 1
		;;
	esac
	if [ -z "$AMDARGS" ]
	then
	    echo "$0: cannot find master map" 1>&2
	    exit 1
	fi
    else
	AMDARGS=""

	if [ "$AM_UTILS_MAP_NET" = true ]
	then
	    AMDARGS="$AMDARGS /net /usr/share/am-utils/amd.net"
	fi

	if [ "$AM_UTILS_MAP_HOME" = true ]
	then
	    AMDARGS="$AMDARGS /home /etc/passwd"
	fi

	AMDARGS="$AMDARGS $AM_UTILS_MAP_OTHERS"
    fi

    if  [ "$AM_UTILS_CLUSTERNAME" != "" ]; then
        AMDARGS="-C \"$AM_UTILS_CLUSTERNAME\" $AMDARGS"
    fi

}

start_amd() {
    pid="`amq -p 2>/dev/null`"
    if [ -n "$pid" ]; then
	echo "Starting automounter: amd is already running" 1>&2
	exit 0
    fi

    # If the hostname is not a FQHN, add the -d command line switch,
    # so that hostnames in the maps are stripped correctly.
    case `hostname` in
	*.*)	dnsdomain=''
		;;
	*)	if test "$AM_UTILS_USE_NIS" = true; then
	          dnsdomainname=`dnsdomainname`
		  if [ "$dnsdomainname" ] ; then
		    dnsdomain="-d $dnsdomainname"
		  else
		    echo "$0: please setup your domainname" 1>&2
		    exit 1
		  fi
		else
		  dnsdomain=''
		fi
		;;
    esac

    get_amd_args

    echo -n "Starting automounter: amd"
    /usr/sbin/amd -F /etc/am-utils/amd.conf $dnsdomain $AMDARGS
    echo "."
}

raze_amd() {
  # This function tries to kill an amd which hasn't exited nicely.
  # This happens really easily, especially if using mount_type autofs

  # Get the currently mounted filesystems
  filesystems=`/bin/tempfile -s .amd`
  if [ $? -ne 0 ]; then
    return 1
  fi

  amq | awk '$2 !~ "root" {print $1}' > $filesystems

  # Kill the daemon
  kill -9 "$pid"

  sleep 10

  if kill -s 0 $pid; then
    rm -f $filesystems
    return 1
  fi

  # Attempt to forcibly unmount the remaining filesystems
  returncode=0
  tac $filesystems | while read fs; do
    umount -l -f "$fs" || returncode=1
    sleep 1
  done

  rm -f $filesystems
  return $returncode
}

stop_amd() {
    pid="`amq -p 2>/dev/null`"
    if [ -z "$pid" ]; then
	echo "Stopping automounter: amd not running" 1>&2
	if [ $# -eq 0 ]
	then
	    exit 0
	else
	    return
	fi
    fi
    echo -n "Requesting amd unmount filesystems: "
    amq | awk '{print $1}' | tac | xargs -r -n 1 amq -u
    echo " done."
    echo -n "Stopping automounter: amd "
    kill -s TERM "$pid"
    # wait until amd has finished; this may take a little bit, and amd can't
    # start while an old one is running
    i=0
    maxsecs=120
    while [ $i -le $maxsecs ] && kill -s 0 $pid >/dev/null 2>&1; do
	echo -n "."
	sleep 1
	i="`expr $i + 1`"
	[ $i -eq 15 ] && echo -n " [will wait `expr $maxsecs - $i` secs more] "
    done
    if kill -s 0 $pid >/dev/null 2>&1
    then
	if raze_amd; then
	  echo " done."
	else
	  echo " failed."
	  exit 1
	fi
    else
	echo " done."
    fi
}

case "$1" in
  start)
	start_amd
	;;

  stop)
	stop_amd
	;;


  restart|force-reload)
	stop_amd -noquit
	start_amd
	;;

  reload)
	# amd tests itself if its map files have changed, so nothing to do here
	;;

  *)
	echo "Usage: /etc/init.d/amd {start|stop|restart|[force-]reload}"
	exit 1
esac

exit 0
