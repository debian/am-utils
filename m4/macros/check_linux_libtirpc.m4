dnl ######################################################################
dnl Check for Linux libtirpc library
AC_DEFUN([AMU_CHECK_LIBTIRPC],[
TIRPC_CPPFLAGS=""
TIRPC_LIBS=""

AC_CHECK_HEADER(tirpc/netconfig.h,[
  TIRPC_CPPFLAGS="-I/usr/include/tirpc"
  AC_DEFINE(HAVE_LIBTIRPC, 1, [Define to 1 if you have libtirpc headers installed])
  AC_CHECK_LIB(tirpc, clnt_tli_create, [TIRPC_LIBS="-ltirpc"], [TIRPC_CPPFLAGS=""])])
  AMU_CFLAGS="$AMU_CFLAGS $TIRPC_CPPFLAGS"
  LIBS="$LIBS $TIRPC_LIBS"
])
